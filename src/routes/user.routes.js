import UserController from '../controller/user.controller';
import AuthController from '../controller/auth.controller';

const UserRoutes = (router) => {

    router.route('/users/login').post(AuthController.login);

    router.route('/users')
        .get(UserController.findAll)
        .post(UserController.create);
    router.route('/users/:id')
        .get(UserController.findOne)
        .post(UserController.findOneGraph);
    //     .put(UserController.update)
    //     .patch(UserController.update)
    //     .delete(UserController.delete)
    router.route('/users/check-email').post(UserController.checkExistEmail);
    router.route('/users/check-username').post(UserController.checkExistUserName);
}

export default UserRoutes;